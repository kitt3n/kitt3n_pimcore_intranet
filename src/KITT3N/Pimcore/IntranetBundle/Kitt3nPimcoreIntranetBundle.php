<?php

namespace KITT3N\Pimcore\IntranetBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class Kitt3nPimcoreIntranetBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/kitt3npimcoreintranet/js/pimcore/startup.js'
        ];
    }
}