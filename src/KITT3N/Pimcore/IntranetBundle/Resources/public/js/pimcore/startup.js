pimcore.registerNS("pimcore.plugin.Kitt3nPimcoreIntranetBundle");

pimcore.plugin.Kitt3nPimcoreIntranetBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.Kitt3nPimcoreIntranetBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("Kitt3nPimcoreIntranetBundle ready!");
    }
});

var Kitt3nPimcoreIntranetBundlePlugin = new pimcore.plugin.Kitt3nPimcoreIntranetBundle();
